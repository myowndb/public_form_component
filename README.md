# MyOwnDB Public Form Web Component

This is a web component to easily embed a responsive [MyOwnDB](https://www.myowndb.com) public form in your website.

# Howto

First ensure you have the component loaded in your page. The javascript to load in your page is the fie under the `src` directory of this git repo.
Copy that file to your website's javascript files directory (in this example it is `js`). You can do it with curl:
```
curl -o js/public_form.js https://gitlab.com/myowndb/public_form_component/-/raw/master/src/public_form.js
```

Then, load that js file in the page where you want to embed the public form:
```
<script src="/js/public_form.js"></script>
```

At this time, you can embed the public form in your website. Look up the URL of your public form in MyOwnDB and use it to set the
`src` attribute of the `myowndb-form` element. For example, this embeds the form used on the [contact page of MyOwnDB.com](https://www.myowndb.com/contact.html):
```
<myowndb-form src="https://app.myowndb.com/app/entities/public_form/838"></myowndb-form>
```

# Example

The `examples` directory gives you an example of how to use the component. You can see the resulting page at [https://myowndb.gitlab.io/public_form_component](https://myowndb.gitlab.io/public_form_component).

# What it does

It embeds an `iframe` whose size is controlled by [iframe resizer](https://github.com/davidjbradshaw/iframe-resizer).
The component loads the minimized version of the iframe resizer library.
